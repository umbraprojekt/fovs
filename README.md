# FOVs

A collection of FOV (field of view) algorithms written by other people, but unavailable via npm. Includes TypeScript typings. Intended for use with the FOV Torture Chamber project.

## Included algorithms

### mrpas-js

Original: https://github.com/domasx2/mrpas-js

Author: Domas Lapinskas

### symmetric recursive shadowcasting

Original: https://gist.github.com/as-f/59bb06ced7e740e11ec7dda9d82717f6

Author: Albert Ford
