export namespace MrpasJs {
	export class Tile {
		public wall: boolean;
		public visible: boolean;
	}
	export class Map {
		public constructor(size: Array<number>);
		public size: Array<number>;
		public tiles: Array<Array<Tile>>;
		public get_tile(pos: Array<number>): Tile | null;
		public reset_visibility(): void;
		public set_visible(pos: Array<number>): void;
		public is_visible(pos: Array<number>): boolean;
		public is_transparent(pos: Array<number>): boolean;
	}
	export function compute_quadrant(map: Map, position: Array<number>, maxRadius: number, dx: number, dy: number): void;
	export function compute(map: Map, position: Array<number>, vision_range: number): void;
}

export declare function SymmetricShadowcasting(
	cx: number,
	cy: number,
	transparent: (x: number, y: number) => boolean,
	reveal: (x: number, y: number) => void
): void;

export namespace ROT {
	export namespace FOV {
		export interface VisibilityCallback { (x: number, y: number, r: number, visibility: number): void }
		export interface LightPassesCallback { (x: number, y: number): boolean }
		export interface Options {
			topology: 4 | 6 | 8;
		}
		export class DiscreteShadowcasting {
			constructor(lightPassesCallback: LightPassesCallback, options?: Partial<Options>);
			compute(x: number, y: number, R: number, callback: VisibilityCallback): void;
		}
		export class PreciseShadowcasting {
			constructor(lightPassesCallback: LightPassesCallback, options?: Partial<Options>);
			compute(x: number, y: number, R: number, callback: VisibilityCallback): void;
		}
		export class RecursiveShadowcasting {
			constructor(lightPassesCallback: LightPassesCallback, options?: Partial<Options>);
			compute(x: number, y: number, R: number, callback: VisibilityCallback): void;
		}
	}
}
